#!/bin/sh

set -ex

toy="toymaker"
item="$1"

git clone https://git.apiote.xyz/git/${toy}.git
cd ${toy}
shellcheck *.sh templates/*
tar cf ${toy}.txz build_images.sh get.sh password.sh toymaker.sh start.sh templates/ log_parser.awk
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')
scp ${toy}.txz deploy@host.containers.internal:toys/${toy}/${item}/${toy}_${timestamp}_${commit}.txz
ssh deploy@host.containers.internal "cp ~/toys/${toy}/${item}/${toy}_${timestamp}_${commit}.txz ~/${toy}.txz"
ssh deploy@host.containers.internal "tar xavf ${toy}.txz"
ssh deploy@host.containers.internal "rm ${toy}.txz"
# ssh deploy@host.containers.internal "doas /etc/init.d/${toy} stop"
# ssh deploy@host.containers.internal "doas /etc/init.d/${toy} start"
