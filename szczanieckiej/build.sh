#!/bin/sh

set -ex

item="$1"

git clone https://git.apiote.xyz/git/szczanieckiej.git
git clone https://git.apiote.xyz/git/traffic.git
(
  cd traffic
  TAG=$(git tag | sort -V | tail -n1)
  git checkout "$TAG"
)
cd szczanieckiej
mk
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')
scp szczanieckiej "deploy@host.containers.internal:toys/szczanieckiej/${item}/szczanieckiej_${timestamp}_${commit}"
# shellcheck disable=SC2029
ssh deploy@host.containers.internal "cp ~/toys/szczanieckiej/${item}/szczanieckiej_${timestamp}_${commit} /tmp/szczanieckiej"
ssh deploy@host.containers.internal "chmod 755 /tmp/szczanieckiej"
ssh deploy@host.containers.internal "doas cp /tmp/szczanieckiej /usr/bin"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/szczanieckiej restart"
ssh deploy@host.containers.internal "rm /tmp/szczanieckiej"
