#!/bin/sh

set -ex

item="$1"

git clone https://github.com/nadoo/glider
cd glider
TAG=$(git tag | sort -V | tail -n1)
git checkout "$TAG"
go build -v -ldflags "-s -w"
scp glider "deploy@host.containers.internal:toys/glider/${item}/glider-${TAG}"
ssh deploy@host.containers.internal "cp ~/toys/glider/${item}/glider-${TAG} /tmp/glider"
ssh deploy@host.containers.internal "chmod a+rwX /tmp/glider"
ssh deploy@host.containers.internal "doas cp /tmp/glider /usr/bin/glider"
ssh deploy@host.containers.internal "rm /tmp/glider"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/glider restart"
