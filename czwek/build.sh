#!/bin/sh

set -ex

item="$1"

git clone --branch master https://git.apiote.xyz/git/Bimba.git
cd Bimba
./gradlew assembleRelease
if ! git show --format='%d' | head -n1 | grep -q tag; then
  tag=$(grep -Eo 'versionName = "[0-9\.]+[^"]*"' app/build.gradle.kts | cut -d '=' -f2 | tr -d ' "')
else
  tag=$(git tag | sort -V | tail -n1)
fi
buildTools=$(ls /root/sdk/build-tools | sort -V | tail -n1)
"/root/sdk/build-tools/$buildTools/apksigner" sign --alignment-preserved --ks "/root/android.jks" --ks-pass "file:/root/keystore_pass" --ks-key-alias bimba --key-pass "file:/root/key_pass" --in app/build/outputs/apk/release/app-release-unsigned.apk --out app/build/outputs/apk/release/app-release-signed.apk
if [ "${ENVIRONMENT:-production}" = 'production' ]; then
  scp app/build/outputs/apk/release/app-release-signed.apk "deploy@host.containers.internal:toys/czwek/${item}/Bimba-${tag}.apk"
  scp app/build/outputs/apk/release/app-release-signed.apk "deploy@host.containers.internal:/srv/www/artifacts/bimba/Bimba-${tag}.apk"
  ssh deploy@host.containers.internal "doas /usr/local/bin/srv_perms"
fi
