#!/bin/sh

set -ex

item="$1"

git clone https://git.apiote.xyz/git/traffic.git
(
  cd traffic
  git switch next
)
git clone https://git.apiote.xyz/git/szczanieckiej.git
cd szczanieckiej
git switch next
mk
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')
scp szczanieckiej "deploy@host.containers.internal:toys/szczanieckiej-next/${item}/szczanieckiej_${timestamp}_${commit}"
# shellcheck disable=SC2029
ssh deploy@host.containers.internal "cp ~/toys/szczanieckiej-next/${item}/szczanieckiej_${timestamp}_${commit} /tmp/szczanieckiej-next"
ssh deploy@host.containers.internal "chmod 755 /tmp/szczanieckiej-next"
ssh deploy@host.containers.internal "doas cp /tmp/szczanieckiej-next /usr/bin"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/szczanieckiej-next restart"
ssh deploy@host.containers.internal "rm /tmp/szczanieckiej-next"
