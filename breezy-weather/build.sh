#!/bin/sh

set -ex

item="$1"

git clone --branch apiote --single-branch --depth 1 https://git.apiote.xyz/git/breezy-weather.git
cd breezy-weather

name="BreezyWeather-"
if ! git show --format='%d' | head -n1 | grep -q tag; then
  commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
  name="$name$commit"
else
  tag=$(git tag | sort -V | tail -n1)
  name="$name$tag"
fi
name="${name}_arm64-v8a.apk"

./gradlew assembleBasicRelease
buildTools=$(ls /root/sdk/build-tools | sort -V | tail -n1)
"/root/sdk/build-tools/$buildTools/apksigner" sign --ks "/root/android.jks" --ks-pass "file:/root/keystore_pass" --ks-key-alias weather --key-pass "file:/root/key_pass" --in app/build/outputs/apk/release/app-standard-arm64-v8a-release-unsigned.apk --out app/build/outputs/apk/release/app-standard-arm64-v8a-release-signed.apk
scp app/build/outputs/apk/standard/release/app-standard-arm64-v8a-release-signed.apk "deploy@host.containers.internal:toys/breezy-weather/${item}/${name}"
