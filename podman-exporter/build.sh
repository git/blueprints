#!/bin/sh

set -ex

item="$1"

git clone https://github.com/containers/prometheus-podman-exporter
cd prometheus-podman-exporter
TAG=$(git tag | sort -V | tail -n1)
git checkout "$TAG"
make binary
scp ./bin/prometheus-podman-exporter "deploy@host.containers.internal:toys/podman-exporter/${item}/prometheus-podman-exporter-${TAG}"
ssh deploy@host.containers.internal "cp ~/toys/podman-exporter/${item}/prometheus-podman-exporter-${TAG} /tmp/podman-exporter"
ssh deploy@host.containers.internal "chmod a+rwX /tmp/podman-exporter"
ssh deploy@host.containers.internal "doas cp /tmp/podman-exporter /usr/bin/podman-exporter"
ssh deploy@host.containers.internal "rm /tmp/podman-exporter"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/podman-exporter restart"
