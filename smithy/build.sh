#!/bin/sh

set -ex

item="$1"

git clone https://git.apiote.xyz/git/smithy.git
cd smithy
TAG=$(git tag | sort -V | tail -n1)
git checkout "$TAG"
make
scp smithy "deploy@host.containers.internal:toys/smithy/${item}/smithy-${TAG}"
ssh deploy@host.containers.internal "cp ~/toys/smithy/${item}/smithy-${TAG} /tmp/smithy"
ssh deploy@host.containers.internal "chmod a+rwX /tmp/smithy"
ssh deploy@host.containers.internal "doas cp /tmp/smithy /usr/bin/smithy"
ssh deploy@host.containers.internal "rm /tmp/smithy"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/smithy restart"
