#!/bin/sh

set -ex

item="$1"

git clone https://git.apiote.xyz/git/Bimba.git
cd Bimba
git checkout develop
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')
./gradlew assembleDebug
if [ "${ENVIRONMENT:-production}" = 'production' ]; then
  scp app/build/outputs/apk/debug/app-debug.apk "deploy@host.containers.internal:toys/czwek-commitly/${item}/Bimba-${timestamp}_${commit}.apk"
fi
