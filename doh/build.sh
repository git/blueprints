#!/bin/sh

set -ex

item="$1"

git clone https://github.com/m13253/dns-over-https
cd dns-over-https
TAG=$(git tag | sort -V | tail -n1)
git checkout "$TAG"
make doh-server/doh-server
scp ./doh-server/doh-server "deploy@host.containers.internal:toys/doh/${item}/doh-server-${TAG}"
ssh deploy@host.containers.internal "cp ~/toys/doh/${item}/doh-server-${TAG} /tmp/doh-server"
ssh deploy@host.containers.internal "chmod a+rwX /tmp/doh-server"
ssh deploy@host.containers.internal "doas cp /tmp/doh-server /usr/bin/doh-server"
ssh deploy@host.containers.internal "rm /tmp/doh-server"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/doh restart"
