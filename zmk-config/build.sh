#!/bin/sh

item="$1"

git clone https://git.apiote.xyz/git/zmk-config.git config-mine

west build -d build/left -b nice_nano_v2 -- -DSHIELD=cradio_left -DZMK_CONFIG=config-mine/config/
west build -d build/right -b nice_nano_v2 -- -DSHIELD=cradio_right -DZMK_CONFIG=config-mine/config/

commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')

if [ "${ENVIRONMENT:-production}" = 'production' ]; then
  scp build/left/zephyr/zmk.uf2 "deploy@host.containers.internal:toys/zmk-config/${item}/left_${timestamp}_${commit}.uf2"
  scp build/right/zephyr/zmk.uf2 "deploy@host.containers.internal:toys/zmk-config/${item}/right_${timestamp}_${commit}.uf2"
fi
