#!/bin/sh

set -ex

item="$1"

git clone https://git.apiote.xyz/git/kosyncsrv.git
cd kosyncsrv
go build
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')
scp kosyncsrv deploy@host.containers.internal:toys/kosync/${item}/kosync_${timestamp}_${commit}
ssh deploy@host.containers.internal "cp ~/toys/kosync/${item}/kosync_${timestamp}_${commit} /tmp/kosync"
ssh deploy@host.containers.internal "chmod 755 /tmp/kosync"
ssh deploy@host.containers.internal "doas cp /tmp/kosync /usr/bin/kosync"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/kosync restart"
