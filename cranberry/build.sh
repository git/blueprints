#!/bin/sh

set -ex

item="$1"

git clone https://git.apiote.xyz/git/cranberry.git
cd cranberry
go build
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')
scp cranberry deploy@host.containers.internal:toys/cranberry/${item}/cranberry_${timestamp}_${commit}
ssh deploy@host.containers.internal "cp ~/toys/cranberry/${item}/cranberry_${timestamp}_${commit} /tmp/cranberry"
ssh deploy@host.containers.internal "chmod a+rwX /tmp/cranberry"
ssh deploy@host.containers.internal "doas cp /tmp/cranberry /usr/bin/cranberry"
ssh deploy@host.containers.internal "rm /tmp/cranberry"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/cranberry restart"
