#!/bin/sh

set -ex

item="$1"

git clone https://github.com/cnt0/goose
cd goose
go build
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')
scp goose deploy@host.containers.internal:toys/goose/${item}/goose_${timestamp}_${commit}
ssh deploy@host.containers.internal "cp ~/toys/goose/${item}/goose_${timestamp}_${commit} /tmp/goose"
ssh deploy@host.containers.internal "chmod 755 /tmp/goose"
ssh deploy@host.containers.internal "doas cp /tmp/goose /usr/bin/goose"
ssh deploy@host.containers.internal "doas setcap cap_ipc_lock+ep /usr/bin/goose"
