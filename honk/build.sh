#!/bin/sh

set -ex

item="$1"

http_proxy=91.107.211.49:8080 hg clone https://humungus.tedunangst.com/r/honk
cd honk
TAG=$(hg tags | head -n2 | tail -n1 | cut -d ' ' -f1)
hg checkout "$TAG"
make
mkdir "honk-$TAG"
cp honk "honk-$TAG/"
cp -r views "honk-$TAG/"
tar cf honk.tar "honk-$TAG/"
# memes/ -> ręcznie # how does it still work?
# honk.db -> ręcznie
# blob.db -> ręcznie

# custom css (views/local.css)
# honk backup to backup
# honk cleanup in cron

scp ./honk.tar "deploy@host.containers.internal:toys/honk/${item}/honk-${TAG}.tar"
ssh deploy@host.containers.internal "cp ~/toys/honk/${item}/honk-${TAG}.tar /tmp/honk.tar"
ssh deploy@host.containers.internal "cd /tmp; tar xf honk.tar"
ssh deploy@host.containers.internal "chmod -R a+rwX /tmp/honk-${TAG}"
ssh deploy@host.containers.internal "mv /tmp/honk-${TAG} /tmp/honk-release"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/honk stop"
ssh deploy@host.containers.internal "doas -u www rm -rf /usr/share/honk/views"
ssh deploy@host.containers.internal "doas -u www honk -datadir /usr/share/honk/ backup /usr/share/honk/backup/"
ssh deploy@host.containers.internal "doas cp /tmp/honk-release/honk /usr/bin/honk"
ssh deploy@host.containers.internal "doas -u www cp -r /tmp/honk-release/views /usr/share/honk/"
ssh deploy@host.containers.internal "rm -rf /tmp/honk-release"
ssh deploy@host.containers.internal "doas -u www honk -datadir /usr/share/honk/ upgrade"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/honk start"
