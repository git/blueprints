#!/bin/sh

set -ex

item="$1"

git clone https://git.apiote.xyz/git/asgard.git
cd asgard
mk
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')
scp asgard deploy@host.containers.internal:toys/asgard/${item}/asgard_${timestamp}_${commit}
ssh deploy@host.containers.internal "cp ~/toys/asgard/${item}/asgard_${timestamp}_${commit} /tmp/asgard"
ssh deploy@host.containers.internal "chmod a+rwX /tmp/asgard"
ssh deploy@host.containers.internal "doas cp /tmp/asgard /usr/bin/asgard"
ssh deploy@host.containers.internal "rm /tmp/asgard"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/asgard restart"
