#!/bin/sh

set -ex

item="$1"

git clone --branch apiote --single-branch --depth 1 https://git.apiote.xyz/git/OSMDashboard.git
cd OSMDashboard
# TODO tag on this commit, else commit hash
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
./gradlew assembleOfflineRelease
buildTools=$(ls /opt/android-sdk-linux/build-tools | sort -V | tail -n1)
mv build/outputs/apk/offline/release/*.apk build/outputs/apk/offline/release/app-offline-release-unsigned.apk
"/opt/android-sdk-linux/build-tools/$buildTools/apksigner" sign --ks "/root/android.jks" --ks-pass "file:/root/keystore_pass" --ks-key-alias osmdashboard --key-pass "file:/root/key_pass" --in build/outputs/apk/offline/release/app-offline-release-unsigned.apk --out build/outputs/apk/offline/release/app-offline-release-signed.apk
scp build/outputs/apk/offline/release/app-offline-release-signed.apk "deploy@host.containers.internal:toys/osmdashboard/${item}/OSMDashboard-${commit}_arm64-v8a.apk"
