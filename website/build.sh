#!/bin/sh

set -ex

item="$1"

git clone https://git.apiote.xyz/git/website.git
cd website
go build
commit=$(git show --pretty=oneline --abbrev-commit --name-only | head -n1 | cut -d ' ' -f 1)
timestamp=$(date '+%Y%m%dT%H%M')
scp website deploy@host.containers.internal:toys/website/${item}/website_${timestamp}_${commit}
ssh deploy@host.containers.internal "cp ~/toys/website/${item}/website_${timestamp}_${commit} /tmp/website"
ssh deploy@host.containers.internal "chmod a+rwX /tmp/website"
ssh deploy@host.containers.internal "doas cp /tmp/website /usr/bin/website"
ssh deploy@host.containers.internal "rm /tmp/website"
ssh deploy@host.containers.internal "doas -u www doas /etc/init.d/website restart"
